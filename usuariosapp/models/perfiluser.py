#Django
from django.db import models
from django.contrib.auth.models import User

#Local models
from .tipoperfil import Tipoperfil

# Create your models here.
class Profiles(models.Model):
    #Proxy model data extends the base data with order information
    user = models.OneToOneField(User, on_delete = models.CASCADE)
    kindUser = models.ForeignKey(Tipoperfil, on_delete=models.DO_NOTHING)
    createdDate = models.DateTimeField(auto_now_add = True)
    modifiedDate = models.DateTimeField(auto_now_add = True)

    def __str__(self):
        #Imprimir el Nombre y no el ID
        return(self.user.username)
    
    class Meta:
        pass
