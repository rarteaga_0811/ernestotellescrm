#Django
from django.db import models

class Tipoperfil(models.Model):
    tipo = models.CharField(max_length=25)
    description = models.CharField(max_length=50)