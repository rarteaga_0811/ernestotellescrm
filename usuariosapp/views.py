from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect #redireccionar a un template
# Create your views here.

# Exception
from django.db.utils import IntegrityError
# Models
from django.contrib.auth.models import User

#Local
from .models import Profiles


@login_required #debe existir una session activa
def logout_view(request):
    logout(request)
    return redirect('/login')

#Función para iniciar sesion
def login_view(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username = username, password = password) #Valida con los usuarios de dbo.auth_user
        if user:
            login(request, user)
            return redirect('/main/')
        else:
            return render(request, 'users/login.html', {'error': 'Usuario y contraseña no validos'})
    return render(
        request,'users/login.html'
    )

