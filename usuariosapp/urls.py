""" Usuarios Urls"""
from django.urls import path

from . import views


urlpatterns = [
    path(route ='login/', view = views.login_view, name='login'),
    path('logout/', views.logout_view),
]

