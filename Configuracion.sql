USE ETELLEZ_LOCAL;
-- SELECT * FROM  etapasEscritura_etapas
--
-- Registro de etapas de escrituracion
--
INSERT INTO  etapasEscritura_etapas VALUES (1,'Registro','El cliente ha sido registrado', 1);
INSERT INTO  etapasEscritura_etapas VALUES(2,'Medicion de propiedad a escrituras y recolección de documentos','Estimado cliente, 
le informamos que a partir de hoy su expediente para el tramite de escritura está completo. 
Estamos a sus órdenes para cualquier duda. Gracias.',1);
INSERT INTO  etapasEscritura_etapas VALUES(3,'Gestion de avaluo','Estimado cliente, le informamos que es día de hoy tramitamos su avaluo catastral, 
la fecha de la visita con el personal de presidencia será el día {}. Estamos a sus ordenes para cualquier duda, Gracias.',1);
INSERT INTO  etapasEscritura_etapas VALUES(4,'Elaboración de la escritura','Estimado cliente, le informamos que acabamos de recibir su avaluo catastral, 
en los próximos días le daremos aviso para programemos la firma de su escritura. 
Estamos a sus órdenes para cualquier duda. Gracias.',1);
INSERT INTO  etapasEscritura_etapas VALUES(5,'Firma','Estimado cliente, le informamos que su fecha de firma será el dia {}, 
le recordamos que nuestro horario de servicio es de 8:00 hrs a 15:00 hrs. 
Estamos a sus órdenes para cualquier duda. Gracias.',1);
INSERT INTO  etapasEscritura_etapas VALUES(6,'Gestion de traslado de dominio','Estimado cliente, le informamos que el traslado de domino de su escritura ha sido solicitado. 
Estamos a sus órdenes par cualquier duda. Gracias',1);
INSERT INTO  etapasEscritura_etapas VALUES(7,'Armado de escritura','Estimado cliente, le informamos que ya contamos con su escritura armada en nuestra oficina, 
le solicitamos pase a corroborar que todos los datos sea correctos para poder proceder al registro de la misma. 
Estamos a sus órdenes para cualquier duda. Gracias',1);
INSERT INTO  etapasEscritura_etapas VALUES(8,'Registro de escritura','Estimado cliente, le informamos que el día de hoy su escrito ingreso al registro público de la propiedad del comiercio. 
Estamos a sus órdenes para cualquier duda. Gracias',1);
INSERT INTO  etapasEscritura_etapas VALUES(9,'Liberación de escritura','Estimado cliente, le informamos que su escritura está terminada. Lo esperamos para entregársela. 
Agradecemos su confianza y preferencia, esperamos tener nuevamente la oportunidad de trabajar para usted.',1);

--
-- Clientes ocupaciones
-- SELECT * FROM clientes_ocupacion
insert into  clientes_ocupacion values(1,'empl.','Ingeniero');
insert into  clientes_ocupacion values(2,'empr','Licenciado');
insert into  clientes_ocupacion values(3,'prof.','Doctor/Doctora');
