"""
 Funciones que funcionan como utilidades adicionales
 o para ahorrar la escritura de código
"""

from etapasEscritura.models import EtapasClientes, Etapas

#WhatsApp
import webbrowser as web
import time

"""
Obtener la etapa Actual de cliente
"""
def etapasActualCliente(clienteModel):
    etapa=''
    try:
        etapascliente = EtapasClientes.objects.filter(cliente=clienteModel)
        lstatuscliente = len(etapascliente)
        statuscliente_id = etapascliente[lstatuscliente-1].etapa_id
        etapa = Etapas.objects.get(id=statuscliente_id)
        return etapa
    except:
        return 0

"""
Obtener la etapa Actual de la escritura
"""
def etapasActualescritura(escrituraModel):
    etapa=''
    etapascliente = EtapasClientes.objects.filter(escritura=escrituraModel)
    lstatuscliente = len(etapascliente)
    statuscliente_id = etapascliente[lstatuscliente-1].etapa_id
    etapa = Etapas.objects.get(id=statuscliente_id)
    return etapa

def enviarMensajeWhatsApp(**kwargs):
    resulr = True
    phone= "+521" + kwargs["numero"]
    txt=kwargs["etapa_msj"]
    try:
        web.open("https://web.whatsapp.com/send?phone="+ phone+"&text="+txt)
        resulr = True
    except:
        resulr = False
        print("error")
    return resulr