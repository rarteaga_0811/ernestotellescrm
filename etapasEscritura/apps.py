#Django
from django.apps import AppConfig

class EtapasEscrituraConfig(AppConfig):
    name = 'etapasEscritura'