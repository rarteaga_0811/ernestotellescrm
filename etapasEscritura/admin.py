from django.contrib import admin

from .models import Etapas
from django.contrib.auth.models import User
from django.contrib.auth.models import Group

admin.site.unregister(User)
admin.site.unregister(Group)

class EtapasAdmin(admin.ModelAdmin):
    fields = ['etapa','mssj', 'es_etapa_final', 'active']
    list_display = ['etapa', 'active', 'es_etapa_final']
    search_fields = ['etapa']
    list_filter = ['active']

admin.site.register(Etapas, EtapasAdmin)