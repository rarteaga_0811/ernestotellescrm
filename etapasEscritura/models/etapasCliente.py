# Django
from django.db import models
from django.contrib.auth.models import User

#Clientes models
from clientes.models import Clientes

#localmodels
from .etapas import Etapas
from clientes.models import Escritura

class EtapasClientes(models.Model):
    cliente = models.ForeignKey(Clientes, on_delete=models.DO_NOTHING)
    escritura = models.ForeignKey(Escritura, on_delete= models.DO_NOTHING)
    etapa = models.ForeignKey(Etapas, on_delete=models.DO_NOTHING)
    fecha = models.DateField(auto_now_add = True)
    viawhatsapp = models.BooleanField(default=True)
    notificacionEmail = models.BooleanField(default=True)
    llamadaTelefonica = models.BooleanField(default=False)
    data_created = models.DateTimeField(auto_now_add=True)
    createdby = models.ForeignKey(User, on_delete=models.DO_NOTHING)
