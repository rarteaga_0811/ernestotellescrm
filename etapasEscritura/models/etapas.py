#django
from django.db import models

class Etapas(models.Model):
    etapa = models.CharField(max_length=70)
    mssj = models.TextField(max_length = 300, blank = True)
    active = models.BooleanField(default=True)
    es_etapa_final = models.BooleanField(default=False)

    def __str__(self):
        """
        """
        return self.etapa

    class Meta:
        verbose_name_plural = "Etapas Escrituración"