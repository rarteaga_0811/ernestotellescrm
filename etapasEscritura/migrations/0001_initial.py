# Generated by Django 3.1.3 on 2022-06-22 00:04

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('clientes', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Etapas',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('etapa', models.CharField(max_length=70)),
                ('mssj', models.TextField(blank=True, max_length=300)),
                ('active', models.BooleanField(default=True)),
            ],
            options={
                'verbose_name_plural': 'Etapas Escrituración',
            },
        ),
        migrations.CreateModel(
            name='EtapasClientes',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('fecha', models.DateField(auto_now_add=True)),
                ('viawhatsapp', models.BooleanField(default=True)),
                ('notificacionEmail', models.BooleanField(default=True)),
                ('llamadaTelefonica', models.BooleanField(default=False)),
                ('data_created', models.DateTimeField(auto_now_add=True)),
                ('cliente', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='clientes.clientes')),
                ('createdby', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to=settings.AUTH_USER_MODEL)),
                ('escritura', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='clientes.escritura')),
                ('etapa', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='etapasEscritura.etapas')),
            ],
        ),
    ]
