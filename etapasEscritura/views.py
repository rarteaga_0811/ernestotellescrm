#Django
from etapasEscritura.models.etapas import Etapas
from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.db.models import Subquery, OuterRef

#Django Rest Framework
from rest_framework import serializers

#Clientes models
from clientes.models import Clientes, Escritura, Direccion, Telefono, Afiliacion, Escritura

#Local models and files
from etapasEscritura.models import EtapasClientes
import etapasEscritura.utils as utils_


"""
 Vista especifica para el cambio de etapa del cliente que recibe como parametro
"""
@login_required
def actualizaEtapa(request, escritura):
    clientes_data = Clientes.objects.all()
    escritura_ = Escritura.objects.filter(id=escritura).values(
        'id',
        'escritura',
        'cliente__id' ,
        'fecha',
        'descripcion',
        'cliente',
        etapaActual = Subquery(Etapas.objects.filter(id = OuterRef('etapa_id')).values_list('etapa'))
    )
    
    for escrituras_ in escritura_:
        escritura_one = Escritura.objects.get(id = int(escrituras_['id']))
        statuscliente_ = escrituras_['etapaActual']
        cliente_ = Clientes.objects.get(id = int(escrituras_['cliente__id']))
        etapaActual = Etapas.objects.get(etapa=statuscliente_)
    
    if request.method == "POST":
        etapaclienteid = request.POST['etapacliente']
        etapascliente_ = Etapas.objects.get(id=int(etapaclienteid))
        etapacliente_ = etapascliente_.etapa
        id_usuario = request.user
        usuario = User.objects.get(id=id_usuario.id)    

        telefono_ = Telefono.objects.get(cliente = cliente_)
        #Obtener el primer numero del cleinte que tenga whatsApp
        #Llamar a la funcion que realizará el envio del msj, parametro etapascliente_.mssj
        mssj_pantalla =""
        if etapascliente_.id == etapaActual.id:       
            mssj_pantalla = "No se actualizó, se ha selecionado la misma etapa"
        else:
            if telefono_.isWhatsApp:
                utils_.enviarMensajeWhatsApp(etapa_msj=etapascliente_.mssj,numero = telefono_.telefono)
            #Crear nueva etapa para cliente
            newetapa = EtapasClientes.objects.create(
                cliente = cliente_,
                escritura = escritura_one,
                etapa = etapascliente_,
                viawhatsapp = telefono_.isWhatsApp,
                notificacionEmail = 1,
                llamadaTelefonica = 0,   
                createdby = usuario,             
            )  

            escritura_one.etapa_id = etapascliente_.id
            escritura_one.save()
        
        cliente_bus = str(cliente_.id) + '-' + cliente_.nombre + ' ' +cliente_.apellidos
        context_request = {'cliente_actualizado':cliente_.nombre,
                           'clientes':clientes_data,
                           'clientebus': cliente_bus,
                           'mssj_pantalla':mssj_pantalla,
                           'escritura': escritura,
                           'etapacliente_actualizado': etapacliente_}
        return render(
        request,'clientes/cambiostatus.html', context_request
    )
    
    #crear funcion para obtener etapa actual del cliente
    try:
        #statuscliente_ = escritura_.etapa #utils_.etapasActualCliente(clienteModel=cliente_)
        statuscliente_ = Etapas.objects.get(etapa = escritura_.etapaActual)
    except:
        statuscliente_ = "Sin cambios"

    etapas = Etapas.objects.all()

    context ={'etapas':etapas,
              'escritura': escritura,
              'etapaCliente_':etapaActual,
    }
    return render(
        request,'clientes/cambiarEtapa.html', context
    )



#Registro cambio de status
"""
Vista para buscar cliente y actualiar la etapa
"""
@login_required
def buscar_cliente(request):
    if request.method == "POST":
        error = ''
        cliente = request.POST["clientenombre"]
        cliente_object_str = cliente.split('-')
        lstatuscliente = 0
        statuscliente_ = []
        whats=""
        afiliacion_ = Afiliacion
        telefono = Telefono
        direccion_ = Direccion
        escrituras_ = None
        escrituraNom = ""
        escrituraNombre =""
        no_escrituras = 0
        try: 
            #crear funcion para obtener etapa actual del cliente
            cliente_ = Clientes.objects.get(id=cliente_object_str[0])
            telefono = Telefono.objects.get(cliente=cliente_)
            direccion_ = Direccion.objects.get(cliente = cliente_)
            escrituras_ = Escritura.objects.filter(cliente = cliente_).values(
                'escritura',
                'fecha',
                'descripcion',
                'cliente',
                etapaActual = Subquery(Etapas.objects.filter(id = OuterRef('etapa_id')).values_list('etapa'))
            )

            
            if escrituras_:
                for escrituras in escrituras_:
                    escrituraNom = escrituras
                    no_escrituras+=1

                escrituraNombre = escrituraNom['escritura']
            else:
                escrituraNombre="Sin escritura"

            
            statuscliente_ = ""
            try:
               statuscliente_ = escrituraNom['etapaActual'] #statuscliente_ = utils_.etapasActualCliente(clienteModel=cliente_)
            except:
                statuscliente_ = "Sin cambios de etapas"
      
            try:
                afiliacion_ = Afiliacion.objects.get(cliente=cliente_)
            except:
                afiliacion_ = None

            if telefono.isWhatsApp == 1: 
                whats = 'Si'
            else:
                whats = 'No'
        except Exception as er:
            error = 'Error en busqueda de cliente - ' + str(er) + ' Favor de contactar al administrador del sistema'

        msj = 'Cliente '+ cliente_object_str[1] + ' encontrado!'
        clientes_data = Clientes.objects.all()
        etapas = Etapas.objects.all()
        ocupacion_ = cliente_.ocupacion.description

        
        context_request = {
            'clientes':clientes_data,
            'afiliacion':afiliacion_,
            'cliente_bus':cliente,
            'ocupacion':ocupacion_,
            'escritura':escrituraNombre,
            'etapaCliente_':statuscliente_,
            'telefono':telefono,
            'iswhats':whats,
            'direccion':direccion_,
            'etapas':etapas,
            'busqueda':'s',
            'error':error,   
            'mssj':msj
    }
        return render(
        request,'clientes/cambiostatus.html', context_request
    )

    clientes_data = Clientes.objects.all()
    context_request = {
        'clientes':clientes_data
    }
    return render(
        request,'clientes/cambiostatus.html', context_request
    )