""" Status Cliente Urls"""
from django.urls import path

#local models
from etapasEscritura  import views

urlpatterns = [
    path(route = 'clientes/cambiodestatus/', view = views.buscar_cliente, name ='clienteStatus'),
    path(route='clientes/cambiarEtapa/<str:escritura>',view = views.actualizaEtapa, name='cambiarEtapa'),
]
