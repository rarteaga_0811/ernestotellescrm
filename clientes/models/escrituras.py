#Django
from django.db import models

#local models
from clientes.models import Clientes

class Escritura(models.Model):
    escritura = models.CharField(max_length=80)
    fecha = models.DateTimeField(auto_now_add=True)
    descripcion = models.CharField(max_length=255)
    monto = models.DecimalField(max_digits=13, decimal_places=2, default=0.00)
    fecha_cierre = models.DateField(blank=True, null=True)
    cliente = models.ForeignKey(Clientes, on_delete=models.DO_NOTHING)
    etapa_id = models.IntegerField()


    def __str__(self):
        return self.escritura
    
    class Meta:
        verbose_name = "Escrituras de clientes"
        verbose_name_plural = "Escrituras de clientes"