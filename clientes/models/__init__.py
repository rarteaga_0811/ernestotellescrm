from .clientes import Clientes
from .ocupaciones import Ocupacion
from .direcciones import Direccion
from .afiliacion import Afiliacion
from .telefonos import Telefono
from .escrituras import Escritura