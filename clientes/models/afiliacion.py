#Django
from django.db import models

#Local models
from .clientes import Clientes

class Afiliacion(models.Model):
    cliente = models.ForeignKey(Clientes, on_delete=models.DO_NOTHING)
    numero = models.CharField(max_length=40)
    tieneTC = models.BooleanField(default=False)
    tieneCredHipotecario = models.BooleanField(default=False)
    tieneCredAuto = models.BooleanField(default=False)


    def __str__(self):
        return self.cliente.nombre + ' ' + self.cliente.apellidos +' - ' + self.numero
    
    class Meta:
        verbose_name = "Afiliaciones de clientes"
        verbose_name_plural = "Afiliaciones de clientes"