#Django
from django.db import models

#Local models
from .clientes import Clientes

class Telefono(models.Model):
    cliente = models.ForeignKey(Clientes, on_delete = models.DO_NOTHING)
    telefono = models.CharField(max_length=40)
    telefonotipoID = models.IntegerField(blank= True)
    isWhatsApp = models.BooleanField(default=True)
    