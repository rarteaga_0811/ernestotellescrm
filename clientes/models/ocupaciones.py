#Django
from django.db import models

class Ocupacion(models.Model):
    name = models.CharField(max_length=20)
    description = models.CharField(max_length=50)
       

    def __str__(self):
        return self.description
    
    class Meta:
        verbose_name_plural = "Ocupaciones"