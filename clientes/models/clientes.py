#Django
from django.db import models
from django.contrib.auth.models import User

#localmodels
from .ocupaciones import Ocupacion


class Clientes(models.Model):
    nombre = models.CharField(max_length=80)
    apellidos = models.CharField(max_length=90)
    fechaNac = models.DateField()
    ocupacion = models.ForeignKey(Ocupacion, on_delete=models.DO_NOTHING)
    createdby = models.ForeignKey(User, on_delete=models.DO_NOTHING)

    def __str__(self):
        return self.nombre + ' ' + self.apellidos
