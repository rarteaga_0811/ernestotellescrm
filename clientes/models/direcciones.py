#Django
from django.db import models

#Local models
from .clientes import Clientes

class Direccion(models.Model):
    cliente = models.ForeignKey(Clientes, on_delete=models.DO_NOTHING)
    email = models.CharField(max_length=80)
    facebook = models.CharField(max_length = 120)
    direccion = models.CharField(max_length=255)
    cp = models.CharField(max_length=12)
    fraccionamiento = models.CharField(max_length = 50)
    ciudad = models.CharField(max_length=80)
    estado = models.CharField(max_length=80)
    paisID = models.IntegerField()


