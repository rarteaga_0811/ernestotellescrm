#Django
from clientes.models import escrituras
from clientes.models.escrituras import Escritura
from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.conf import settings
from email.mime.image import MIMEImage
from django.db.models import Subquery, OuterRef

#Email templates
from django.template.loader import get_template
from django.core.mail import EmailMultiAlternatives

from datetime import date
from datetime import datetime


from rest_framework import serializers

#Local models
from clientes.models import Clientes, Ocupacion, Telefono, Direccion
from clientes.models import Escritura
from etapasEscritura.models import EtapasClientes, Etapas
import etapasEscritura.utils as utils_


"""
Pantalla principal
"""
@login_required #debe existir una session activa
def index(request):
    return render(request,"main.html")

"""
Vista para actualizar información de clientes
"""
@login_required
def editar_cliente(request, cliente):
    cliente_object_str = cliente.split('-')
    cliente_ = Clientes.objects.get(id=cliente_object_str[0])
    ocupaciondata = Ocupacion.objects.all()
    ocupacioncliente = Ocupacion.objects.get(id=cliente_.ocupacion.id)
    telefonodata = Telefono.objects.get(cliente=cliente_)
    direcciones = Direccion.objects.get(cliente=cliente_)

    if telefonodata.telefonotipoID == 1:
        tipotelid_ = 1
        tipotel_="Cel"
    if telefonodata.telefonotipoID == 2:
        tipotelid_ = 2
        tipotel_="Casa"

    
    if request.method == "POST":
        si_no = 0
        cliente_object_str = cliente.split('-')
        cliente_ = Clientes.objects.get(id=cliente_object_str[0])
        _ocupacion = request.POST['ocupacion']
        ocupacioncliente = Ocupacion.objects.get(name=_ocupacion)
        #Cliente info
        cliente_.nombre = request.POST['username']
        cliente_.apellidos = request.POST['apellidos']
        cliente_.ocupacion = ocupacioncliente
        cliente_.save()
        #Telefonos
        telefono_ = Telefono.objects.get(cliente = cliente_)
        if request.POST['tienewhats1'] == 's':
            whats = 1
        else:
            whats = 0
        telefono_.telefono = request.POST['telefono']
        telefono_.telefonotipoID = request.POST['tipotel']
        telefono_.isWhatsApp = whats
        telefono_.save()
        if request.POST['tipotel'] == 1:
            tipotelid_ = 1
            tipotel_="Cel"
        if request.POST['tipotel'] == 2:
            tipotelid_ = 2
            tipotel_="Casa"
        
        #direccionaes
        direcciones.email = request.POST['email']
        direcciones.facebook = request.POST['facebook']
        direcciones.direccion = request.POST['direccion']
        direcciones.fraccionamiento = request.POST['fraccionamiento']
        direcciones.cp = request.POST['cp']
        direcciones.ciudad = request.POST['ciudad']
        direcciones.estado = request.POST['estado']
        direcciones.save()
        
        #Se realiza actualizacion?
        si_no = 1
        context_request={'actualizado': si_no,
                         'cliente': cliente,
                         'cliente_data': cliente_,
                         'ocupacioncliente':ocupacioncliente,
                         'ocupacion':ocupaciondata,
                         'telefononum':telefono_,
                         'direcciones':direcciones,
                         'tipotelefonodata':{'id':tipotelid_,
                                             'telefono': telefono_.telefono,
                                             'tipodetelefono':tipotel_,
                                             'whats':whats},
                         'tipotelefono':[{'id':'1',
                                          'tipodetelefono':'Cel'},
                                         {'id':'2',
                                          'tipodetelefono':'Casa'},
                                        ],}
        return render(request,'clientes/editarcliente.html',context_request)

    context_request={'cliente':cliente,
                     'cliente_data': cliente_,
                     'ocupacioncliente':ocupacioncliente,
                     'ocupacion':ocupaciondata,
                     'telefononum':telefonodata,
                     'tipotelefonodata':{'id':tipotelid_,
                                         'tipodetelefono':tipotel_,
                                         'whats':telefonodata.isWhatsApp},
                     'tipotelefono':[{'id':'1',
                                   'tipodetelefono':'Cel'},
                                   {'id':'2',
                                   'tipodetelefono':'Casa'},
                                   ],
                     'direcciones':direcciones, }
    return render(request,'clientes/editarcliente.html',context_request)


"""
 Vista que mostrará el historial de cambios de etapas, por fecha, cliente o etapa
"""
def historial_etapas(request):
    clientes = Clientes.objects.all()
    etapas = Etapas.objects.all()
    historial = []
    historial_ = []
    historial_0 = []
    id_usuario = -1

    if request.method == "POST":
        cliente = request.POST['clientenombre1']
        fecha_ = request.POST['fecha']
        etapa_ = request.POST['etapacliente']
        #Validar si se manda etapa
        try:
            etapas_ = Etapas.objects.get(id = int(etapa_))
            netapa = 1
        except:
            netapa = 0
        
        if cliente and netapa==0:
            cliente_object_str = cliente.split('-')
            cliente_ = Clientes.objects.get(id=cliente_object_str[0])
            historial = EtapasClientes.objects.filter(cliente = cliente_)

            for historial in historial:
                telefono = Telefono.objects.get(cliente = historial.cliente)
                if historial.viawhatsapp == True:
                    whats = "Si"
                else:
                    whats = "No"
                
                usuario = User.objects.get(id=historial.createdby.id)
                    
                historial_.append( {'fecha':historial.fecha,
                                    'cliente':historial.cliente.nombre + ' ' + historial.cliente.apellidos,
                                    'etapa':historial.etapa.etapa,
                                    'escritura':historial.escritura.escritura,
                                    'telefono':telefono.telefono,
                                    'whatsapp':whats,
                                    'createdby':usuario.username,})
        
        #Si se busca por fecha y usuario
        if fecha_ and netapa == 0:
            if historial:  
                for historial_ in historial_:
                    telefono = ""
                    whats =""
                    if str(historial_['fecha']) == str(fecha_):
                        historial_0.append({'fecha':historial_['fecha'],
                                            'cliente':historial_['cliente'],
                                            'escritura':historial_['escritura'],
                                            'etapa':historial_['etapa'],
                                            'telefono':historial_['telefono'],#telefono,
                                            'whatsapp':historial_['whatsapp'],
                                            'createdby':historial_['createdby'],})
                        historial_ =[]
                        historial_ = historial_0
                        
            else: #Si solo se busca por fecha
                historial = EtapasClientes.objects.filter(fecha=fecha_)
                for historial in historial:
                    telefono = Telefono.objects.get(cliente = historial.cliente)
                    if telefono.isWhatsApp == True:
                        whats = "Si"
                    else:
                        whats = "No"
                    usuario = User.objects.get(id=historial.createdby.id)
                    historial_.append({'fecha':historial.fecha,
                                       'cliente':historial.cliente.nombre + ' '+historial.cliente.apellidos,
                                       'escritura':historial.escritura.escritura,
                                       'etapa':historial.etapa.etapa,
                                       'telefono':telefono.telefono,
                                       'whatsapp':whats,
                                       'createdby':usuario.username})
        
        #La busqueda por etapa ignora las demas campos de busqueda
        if netapa >= 1:
            historial =[]
            fecha_=""
            cliente=""
            
            
            historial = Escritura.objects.filter(etapa_id = etapa_)
            for historiale in historial:
                cliente_ = Clientes.objects.get(id=historiale.cliente_id)
                try:

                    telefonocli = Telefono.objects.get(cliente = cliente_)
                    etapascliente_ = EtapasClientes.objects.filter(escritura =historiale, etapa_id = etapa_).last()
                    statusclientefecha = etapascliente_.data_created
                    createby = etapascliente_.createdby
                    iswhats = etapascliente_.viawhatsapp
                    if iswhats == True:
                        whats = "Si"
                    else:
                        whats = "No"
                    historial_.append({'fecha':statusclientefecha,
                                        'cliente':cliente_.nombre + ' ' + cliente_.apellidos,
                                        'escritura':etapascliente_.escritura,
                                        'etapa':etapascliente_.etapa,
                                        'telefono':telefonocli.telefono,
                                        'whatsapp':whats,
                                        'createdby':createby})
                except Exception as err:
                    print("error buscar por etapa")
                    print(err)
                

        #Argumentos formulario html         
        context_data = {'clientes':clientes,
                        'fecha':fecha_,
                        'etapas':etapas,
                        'etapadata':historial_,
                        'clientebus':cliente}
        return render(request, 'clientes/historialEtapas.html', context_data)
    context_data = {'clientes':clientes,
                    'etapas':etapas,}
    return render(request, 'clientes/historialEtapas.html',context_data)


"""
Vista para registrar un nuevo cliente
"""
@login_required #debe existir una session activa
def registra_cliente(request):
    if request.method == 'POST':
        #Recopilando info del fomulario html
        _user = request.user
        usuario = User.objects.get(id=_user.id)

        clientenombre = request.POST['clientenombre']
        apellidoscliente = request.POST['apellidoscliente']
        fechanacimiento = request.POST['fechanacimeinto']
        ocupacion = request.POST['ocupacion'] #Obtener codigo (ing,lic, etc)
        #Telefonos
        telefono1 = request.POST['telefono1']
        tipotel1 = request.POST['tipotel1']
        tienewhats = request.POST['tienewhats1']
        #Direcciones
        email = request.POST['email']
        facebook = request.POST['facebook']
        direccion = request.POST['direccion'] #calle y numero
        fraccionamiento = request.POST['fraccionamiento']
        ciudad = request.POST['ciudad'] #regresa CiudadID
        estado = request.POST['estado'] #regresa estadoID
        paisid = request.POST['pais'] #regresa paisid, Mexico por default
        codigopostal = request.POST['cp']

        if tienewhats == 's':
            tienewhats_ = True
        
        if tienewhats == 'n':
            tienewhats_ = False
        
        if email:
            tieneemail = True
        else:
           tieneemail = False
            
        ocupacionmodel = Ocupacion.objects.get(name=ocupacion)
        
        
        clientemodel = Clientes.objects.create(
            nombre = clientenombre,
            apellidos = apellidoscliente,
            fechaNac = fechanacimiento,
            ocupacion = ocupacionmodel,
            createdby = usuario,
        )
        
        
        if telefono1:  
            telefonosmodel = Telefono.objects.create(
                cliente = clientemodel,
                telefono = telefono1,
                telefonotipoID = tipotel1,
                isWhatsApp = tienewhats_,
            )

        direcciones = Direccion
        direccionesmodel = direcciones.objects.create(
            cliente = clientemodel,
            email = email,
            facebook = facebook,
            direccion = direccion,
            fraccionamiento = fraccionamiento,
            ciudad = ciudad,
            estado= estado,
            paisID = paisid,
            cp = codigopostal,
        )
        
        #Validar que si tiene email y si tiene whts app su telefono
        #Se contactará via llamada telefonica o por correo electronico
        #Si no cuenta con correo o whatsApp relizar llamada telefonica
        #habilitar y desabilitar componentes e indicar si tiene o no whatsApp o correo.
        
        #
                
                
        ocupaciondata = Ocupacion.objects.all()
        clientesaved = 'Se registro correctamente a '+ clientenombre + ' '+  apellidoscliente
        #clientesaved.format(clientenombre, apellidoscliente)
        context = {'mssj':clientesaved,
                   'clientenombre':clientenombre,
                   'nuevocliente':'si',
                   'ocupacion':ocupaciondata,
                   'tipotelefono':[{'id':'1',
                                   'tipodetelefono':'Cel'},
                                   {'id':'2',
                                   'tipodetelefono':'Casa'},
                                   ],
                   }
        return render(
            request, 'clientes/registroClientes.html', context

        )
    #Obtener el perfil del usuarios Admin/solo lectura
    ocupaciondata = Ocupacion.objects.all()
    context_request = {'perfil':'',
                       'submit':'si',
                       'ocupacion':ocupaciondata,
                       'tipotelefono':[{'id':'1',
                                   'tipodetelefono':'Cel'},
                                   {'id':'2',
                                   'tipodetelefono':'Casa'},
                                   ],
                       }
    return render( #acciones al cargar la pagina
        request,'clientes/registroClientes.html',context_request)


"""
Vista para crear una nueva escritura de un cliente
"""
@login_required #debe existir una session activa
def crear_escritura(request,cliente):
    context = {}
    template = "clientes/crear_escritura.html"
    #Recopilando info del fomulario html
    _user = request.user
    usuario = User.objects.get(id=_user.id)
    cliente_object_str = cliente.split('-')
    clientemodel = Clientes.objects.get(id=cliente_object_str[0])
    telefono_ = Telefono.objects.get(cliente = clientemodel)
    direccion_ = Direccion.objects.get(cliente=clientemodel)
    tieneemail = direccion_.email
    tieneemail_ = True

    context ={
        'cliente_nombre': cliente,
    }
    
    if request.method == 'POST':
        nombreEscritura = request.POST['nombreEscritura']
        descripcion_ = request.POST['descEscritura']
        monto = request.POST['montoEscritura']
        
        etapa_ = Etapas.objects.get(id=1)
        newEscritura = Escritura.objects.create(
            escritura = nombreEscritura,
            descripcion = descripcion_,
            cliente = clientemodel,
            monto = monto,
            etapa_id = etapa_.id
        )
        
        if tieneemail != "":
            tieneemail_ = True
        else:
            tieneemail_ = False
        
        llamada = False
        if telefono_.isWhatsApp == False and tieneemail_ == False:
            llamada = True

        #etapa de registro
        EtapasClientes.objects.create(
                cliente = clientemodel,
                escritura =  newEscritura,
                etapa = etapa_,
                viawhatsapp = telefono_.isWhatsApp,
                notificacionEmail = tieneemail_,
                llamadaTelefonica = llamada,
                createdby = usuario,           
            )
        mssj = "Se registro nueva escritura"
        context ={
            'cliente_nombre': cliente,
            'mssj': mssj
            }
        return render(request,template, context)

    return render(request,template, context)


"""
 Vista para mostrar escrituras
"""
@login_required #debe existir una session activa
def mostrar_Escrituras(request,cliente):
    context = {}
    template = "clientes/mostrar_escrituras.html"
    _user = request.user
    usuario = User.objects.get(id=_user.id)
    cliente_object_str = cliente.split('-')
    clientemodel = Clientes.objects.get(id=cliente_object_str[0])

    escrituras_ = Escritura.objects.filter(cliente = clientemodel).values(
                'id',
                'escritura',
                'fecha',
                'monto',
                'descripcion',
                etapaActual = Subquery(Etapas.objects.filter(id = OuterRef('etapa_id')).values_list('etapa'))
            )

    context ={
        'escrituras': escrituras_,
        'cliente_bus': cliente
    }

    return render(request, template, context)


#Funcion Complemento envio de correos
def send_mail(email, asunto):
    context = {'mail':email}
    template = get_template('email_templates/hola_bienvenido.html')
    content = template.render(context)
    e_mail = EmailMultiAlternatives(
        asunto,
        'Email ELAA',
        settings.EMAIL_HOST_USER,
        [email]
    )
    e_mail.attach_alternative(content,'text/html')
    #Anexar imagenes
    image_file = open('./static/img/star-wars.jpg', 'rb')
    msg_image = MIMEImage(image_file.read())
    image_file.close()

    msg_image.add_header('Content-ID', '<image1>')
    e_mail.attach(msg_image)
    e_mail.send()
    print('se envio el correo')




#Envio de correos
def sendEmail(request):
    asunto_ =""
    if request.method == 'POST':
        mail = request.POST['mail']
        asunto_ = "Correo prueba 22"
        send_mail(mail, asunto_)

    return render(request,'email_templates/index_email.html')
    