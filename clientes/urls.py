""" Usuarios Urls"""
from django.urls import path

#local models
from clientes import views

urlpatterns = [
    path(route ='clientes/registrarcliente/', view = views.registra_cliente, name='registroclientes'),
    path(route ='clientes/editarcliente/<str:cliente>',view= views.editar_cliente, name='editarclientes'),
    path(route ='clientes/historialEtapas/', view=views.historial_etapas, name='historialEtapas'),
    path(route ='clientes/registrarEscritura/<str:cliente>', view=views.crear_escritura, name = "crearEscritura"),
    path(route ='clientes/mostrarEscrituras/<str:cliente>', view=views.mostrar_Escrituras, name = 'mostrarEscrituras'),
    #Prueba de coreo electrónico
    path(route ='clientes/enviarEmail',view=views.sendEmail, name='sendEmail'),
    path("main/",views.index, name="index"),
]
