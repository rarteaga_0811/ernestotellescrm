from django.contrib import admin

from .models import Ocupacion, Afiliacion, Escritura

class OcupacionAdmin(admin.ModelAdmin):
     fields = ['name','description']
     search_fields = ['name','description']

class AfiliacionAdmin(admin.ModelAdmin):
     list_select_related = ['cliente']
     fields = ['cliente','numero','tieneTC','tieneCredHipotecario','tieneCredAuto']
     search_fields = ['cliente__nombre','numero',]

class EscriturasAdmin(admin.ModelAdmin):
     list_select_related = ['cliente']
     fields = ['escritura','monto', 'cliente','fecha','fecha_cierre']
     list_display = ['escritura','cliente','monto','fecha','fecha_cierre']
     search_fields = ['escritura']
     readonly_fields = ['fecha']



admin.site.register(Ocupacion, OcupacionAdmin)
admin.site.register(Afiliacion, AfiliacionAdmin)
admin.site.register(Escritura, EscriturasAdmin)